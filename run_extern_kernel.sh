#! /usr/bin/env bash
# Script based on the script given in PNL and some options from:
# https://www.collabora.com/news-and-blog/blog/2017/01/16/setting-up-qemu-kvm-for-kernel-development/

IMG='qemu-image.img'
KERNEL='../linux/arch/x86/boot/bzImage'

# NOTE The mount tag is share because in /etc/fstab of the image the tag is
# share and the destination is /root/share.
VIRTFS='-virtfs local,path=..,mount_tag=share,security_model=passthrough,readonly,id=share'

# Set KDB to activate kernel "debugging mode".
# KDB=1

# Set REMOTE if the VM is run through ssh.
# The VM's output will not be redirected into terminal and it will run in
# background.
# REMOTE=1

# Since qemu-img.img contains one partition whom type is ext4 we need to give it
# as root. The device used is /dev/sda because we use scsi.
# NOTE If something does not work it can be good to tweak /dev/sda to /dev/sda1
# or /dev/vda and /dev/vda1.
# WARNING It is also a good thing to explicitly write the 'rw' so the partition
# can be written.
# The console option is to print directly the ouput of qemu in the terminal.
CMDLINE='root=/dev/sda rw console=ttyS0 log_buf_len=50M'

if [ ${KDB} ]; then
	# kgdb will be in another tty and the kernel will run only when kgdb is run.
# 	CMDLINE+=' kgdboc=ttyS1 kgdbwait nokaslr'
	# To use gdbserver/qemu we need to deactivate the kernel address space
	# layout randomization or gdb will not be able to set breakpoint and
	# know where in the code it is.
	CMDLINE+=' nokaslr'
fi

GRAPHIC='-nographic'

# If we run this script on a remote computer we need to daemonize qemu.
# For more explaination about the two options (-daemonize and -nographic) see
# comments below.
if [ ${REMOTE} ]; then
	GRAPHIC='-daemonize'
fi

# -drive file=${IMG},media=disk,id=hdd,index=0,if=none,cache=none,discard=unmap:
# Use $IMG as disk, id to link with scsi is hdd, index 0 is the same as using
# -hda, access to it will be done through virtio-scsi, access will not be cached
# by the host and the drive will support discarding block (blocks released by
# the guest will be release by the host so the image can shrink with fstrim).
# Information about unmap were found here:
# https://chrisirwin.ca/posts/discard-with-kvm/
# -smp 4: Give 4 cpu cores to qemu.
# -m 3G: Give 3GB of memory to qemu.
# -net user,hostfwd=tcp::10022-:22,hostfwd=tcp::12375-:2375,hostfwd=tcp::8080-:8080,hostfwd=tcp::8081-:8081:
# This command forward guest ports to host ports.
# The guest port 22 is forwarded to host port 10022, this is mandatory to permit
# ssh connection from the host to the guest.
# The guest port 2375 is forwarded to host port 12375, this port is used to
# expose the docker socket so it is possible to run container inside the guest
# from the host. ALERT This is really not safe and should only be used for
# experiment.
# The guest ports 8080 and 8081 ae forwarded to the same host port. These
# ports will be used by host container to communicate with guest containers.
# -net nic: I think that qemu will add this option by default but giving the
# previous option seems to modify this behavior. Without this option there is no
# internet interface in virtual machine.
# -device virtio-rng-pci: Add a virtio random device which is connected through
# PCI. Add this option permit to produce hardware entropy and avoid docker to
# block on getrandom(). The information was found on:
# https://github.com/rapido-linux/rapido/issues/44#issuecomment-393136542
# -device virtio-scsi-pci: This devices permit to use a para-virtualized scsi
# controller. It seems to be mandatory to be able to release blocks freed in
# guest in the host.
# -device scsi-hd,drive=hdd: Indicates that hdd is an scsi device. All
# documentation on scsi related options was found here:
# https://fedoraproject.org/wiki/Features/virtio-scsi
# -s: This option is a shorthand to '-gdb tcp::1234' which open a gdbserver on
# TCP port 1234. To connect to it, simply run gdb on host, use file to load what
# you want to debug (e.g. file vmlinux) and connect to gdbserver with 'target
# remote :1234'.
# [-S: This option will not start CPU at VM startup. It can be used to attach
# gdb at the startup of the VM. Then by issuing continue the CPU will start. So
# this option permit debugging from VM startup.]
# -display none: Do not display external display (in the sense that it does not
# open a new window to display qemu output).
# -nographic: Do not use an external window, qemu output is redirected on the
# console.
# -daemonize: Qemu will be daemonized, it is approximately equal to be run in
# background.
# -enable-kvm: Speed up the virtualization by using KVM.
# -kernel: Use this kernel in place of distro/image kernel.
# -append: Append this to the kernel command line. WARNING It is better to put
# this option as the last one.
qemu-system-x86_64 ${FLAGS} \
	-drive file=${IMG},media=disk,id=hdd,index=0,if=none,cache=none,discard=unmap \
	${VIRTFS} \
	-smp 4 \
	-m 3G \
	-net user,hostfwd=tcp::10022-:22,hostfwd=tcp::12375-:2375,hostfwd=tcp::8080-:8080,hostfwd=tcp::8081-:8081 \
	-net nic \
	-chardev socket,id=docker,path=/var/run/docker.sock \
	-device virtio-rng-pci \
	-device virtio-scsi-pci \
	-device scsi-hd,drive=hdd \
	-s \
	-display none \
	${GRAPHIC} \
	-enable-kvm \
	-kernel "${KERNEL}" \
	-append "${CMDLINE}"