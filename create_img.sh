#! /usr/bin/env bash
# The following recipe was taken from:
# https://www.collabora.com/news-and-blog/blog/2017/01/16/setting-up-qemu-kvm-for-kernel-development/

IMG='qemu-image.img'
DIR='mount-point'

# The debian 9.11 image url.
# See a comment below to know why we need this.
DEBIAN_URL='https://cdimage.debian.org/mirror/cdimage/archive/9.11.0/amd64/jigdo-bd/debian-9.11.0-amd64-BD-1.jigdo'

# TODO Add install_docker.sh.
INIT_SCRIPTS='setup_vm.sh'

if [ $EUID -ne 0 ]; then
	echo "${0} must be run as root!" 1>&2

	exit 1
fi

# The argument "mom likes" (or "am i") is a sort of an easter egg who will print
# the user of shell where this script was run. The call to awk permit to clean
# the output of who and just get the username.
real_user=$(who mom likes | awk '{print $1}')

# Create a 25GB raw image so experiment with 8 containers can run without
# problem.
qemu-img create -f raw $IMG 25G

# # Format the image as ext4.
mkfs.ext4 $IMG

mkdir $DIR
mount -o loop $IMG $DIR

# We will install debian stretch 9.11 in it.
# To get the exact 9.11 packages list, we need to get its snapshot.
# snapshot.debian.org contains the packages for a given date.
# We extract it from the jigdo file.
# Jigdo file contains list of files to download to build an ISO.
# Specially, it contains the snapshot exact URL.
#
# This trick was taken from:
# https://superuser.com/a/1313792
snapshot=$(wget -q -O - $DEBIAN_URL | gunzip | awk -F= '/snapshot.debian.org/ {print $2}')

# We can now install debian 9.11 based on its snapshot.
debootstrap --arch amd64 stretch $DIR $snapshot

# Copy some initialization script in the chrooted environment so they can be
# run from it.
for i in $INIT_SCRIPTS; do
	cp $i $DIR
done

# WARNING Redeclare locally the SHELL value to avoid problem with zsh when
# chrooting.
SHELL='/bin/sh'

# Call those scripts.
cat << EOF | chroot $DIR
	for i in $INIT_SCRIPTS; do
		bash $i
	done
EOF

# Clean the scripts previously copied.
for i in $INIT_SCRIPTS; do
	rm $DIR/$i
done

# Configure openssh-server to be able to login as root without password.
mkdir $DIR/root/.ssh
# Add the host public key given to the authorized_keys.
cat /home/$real_user/.ssh/id_rsa.pub >> $DIR/root/.ssh/authorized_keys
# Authorize root login without password.
perl -pi -e 's/#PermitRootLogin prohibit-password/PermitRootLogin without-password/' $DIR/etc/ssh/sshd_config

umount $DIR
rmdir $DIR

# Change the owner of image from root to the real user who uses sudo to run this
# script.
chown $real_user $IMG

echo -e "Image seems ready!\nYou just need to run install_docker.sh when in it."
