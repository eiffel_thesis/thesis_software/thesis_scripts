#! /usr/bin/env bash
# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
# This script is called by create_img.sh to setup the virtual machine to be
# ready to use.

# WARNING This is a quite a magic value but it seems to work.
# To obtain it just run 'ip address' and the one corresponding to link/ether.
ETHER='enp0s3'

if [ $EUID -ne 0 ]; then
	echo "This script must be run as root!" 1>&2

	exit 1
fi

# Delete the root password.
passwd -d root

# Permits read and execute for others in the root directory.
# This is useful when you mount the image to get files which are store in /root.
chmod 755 /root

# Change the hostname to avoid confusion and being sure to be in the VM.
echo 'vm' > /etc/hostname

# Change /etc/network/interfaces so Internet can be used directly after
# connection.
echo -e "# The loopback network interface\nauto lo\niface lo inet loopback\n\n# The primary network interface\nallow-hotplug ${ETHER}\niface ${ETHER} inet dhcp" > /etc/network/interfaces

# Add an entry to fstab to be able to use virtfs 9p shared folder.
# In run_extern_kernel the mount_tag is shared because this is that we write in
# the fstab.
echo "share   /root/share     9p      trans=virtio,version=9p2000.L   0       1" > /etc/fstab

# Run fstrim -av each day to release on host blocks which were released in
# guest. The documentation was found here:
# https://linuxtrack.net/viewtopic.php?id=2281
perl -pi -e 's/week$/day/' /usr/share/doc/util-linux/examples/fstrim.timer
perl -pi -e 's/weekly/daily/' /usr/share/doc/util-linux/examples/fstrim.timer
cp /usr/share/doc/util-linux/examples/fstrim.{timer,service} /etc/systemd/system/
systemctl enable fstrim.timer
systemctl start fstrim.timer

# WARNING This update seems needed otherwise below install will fail.
apt-get -qy update

# Install some useful softwares:
# * man: It is always useful to have the man installed.
# * bash-completion: It is always useful to have a shell that complete your
# commands.
# * python3-pip: It is needed below.
# * lsb_release: Oddly, it is not installed by debootstrap and we need it in
# install_docker.sh.
# * openssh-server: It is needed to connect with ssh inside the VM.
# * trace-cmd: It is needed by experiments.
apt-get -y install man bash-completion python3-pip lsb-release openssh-server trace-cmd

# Install docker python3 API version 4.0.2
pip3 install docker==4.0.2