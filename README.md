# Collection of script to build and run VM

This repository contains a collection of script to build reproducible VM with docker installed.
The VM are based on debian 9.

## Getting started

To build the VM and install docker use the following commands:

```bash
# Create the VM
user@home$ sudo bash create_image.sh
# Run the VM with qemu.
user@home$ bash run_extern_kernel.sh
# Install docker from the VM.
root@vm# bash share/scripts/install_docker.sh
```

You should now be able to run docker.

## VM creation

The script `create_image.sh` creates a raw qemu image of 25GB and install debian 9.11 in it.
This script calls `setup_vm.sh` which installs other software and add `share` to fstab.

`share` is a special directory which permits seeing host files from the VM.
The files are read-only and corresponding to the tree above the execution of `run_extern_kernel.sh`.

## VM run

Before running the VM with `run_extern_kernel.sh` you need to compile a linux kernel and set `KERNEL` variable.
Maybe you will need to modify the values of `-smp` and `-m` to change the amount of CPU cores and memory available for the VM.

## Docker installation

To install docker inside the VM, the script `install_docker.sh` should be run.
It installs docker 19.03.

## Authors

**Francis Laniel** [<francis.laniel@lip6.fr>](francis.laniel@lip6.fr)

## License

This project is licensed under the GPLv2 License.